<?php

namespace Drupal\smart_date_popup\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\date_popup\DatePopupTrait;
use Drupal\smart_date\Plugin\views\filter\Date as SmartDate;

/**
 * Date/time views filter, with granularity and a date popup.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("date")
 */
class Date extends SmartDate {

  use DatePopupTrait;

  /**
   * {@inheritdoc}
   */
  public function buildExposedForm(&$form, FormStateInterface $form_state) {
    parent::buildExposedForm($form, $form_state);
    $this->applyDatePopupToForm($form);
  }

}
